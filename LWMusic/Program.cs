﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace LWMusic
{
    class Program
    {
        static void Main(string[] args)
        {
            Composition composition = new Composition();

            string album = "Горгород";
            composition.AddDisk(album);

            string artist = "oxxxymiron";
            composition.AddSong(album, "Не с начала", artist);
            composition.AddSong(album, "Кем ты стал", artist);
            composition.AddSong(album, "Всего лишь писатель", artist);
            composition.AddSong(album, "Переплетено", artist);
            composition.AddSong(album, "Колыбельная", artist);
            composition.AddSong(album, "Полигон", artist);
            composition.AddSong(album, "Накануне", artist);

            album = "Акустический альбом";
            composition.AddDisk(album);

            artist = "Король и Шут";
            composition.AddSong(album, "Кукла Колдуна", artist);
            composition.AddSong(album, "Наблюдатель", artist);
            composition.AddSong(album, "Бедняжка", artist);
            composition.AddSong(album, "Прыгну со скалы", artist);
            composition.AddSong(album, "Тяни!", artist);

            composition.PrintComposition();
            composition.PrintDisk("Горгород");

            composition.SearchArtist("Король и Шут");
            
            Console.ReadKey();
        }
    }
}
